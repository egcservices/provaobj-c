//
//  DetailViewController.m
//  AvaliacaoPDMiOS
//
//  Created by EGC on 02/09/17.
//  Copyright © 2017 Ibratec. All rights reserved.
//

#import "DetailViewController.h"
#import <UIImageView+AFNetworking.h>
#import <AFNetworking.h>
#import <SVProgressHUD.h>

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self centerMap];
    
    if (![self.comment[@"user"] isKindOfClass:[NSNull class]]){
        self.lblName.text = self.comment[@"user"] ;
    }
    
    if (![self.comment[@"content"] isKindOfClass:[NSNull class]]){
        self.lblComent.text = self.comment[@"content"] ;
    }
    
    if (![self.comment[@"created_at"] isKindOfClass:[NSNull class]]){
        self.lblData.text = self.comment[@"created_at"] ;
    }
    
    if (![self.comment[@"uploaded_image"] isKindOfClass:[NSNull class]]){
        NSURL	*filePath	=	[NSURL URLWithString: self.comment[@"uploaded_image"]];
        [self.myImgComent setImageWithURL:filePath placeholderImage:[UIImage imageNamed:@"place_user"]];
    }
}

-(void) centerMap{
    
    if (![self.comment[@"lat"] isKindOfClass:[NSNull class]]
        && ![self.comment[@"lng"] isKindOfClass:[NSNull class]]){
        
        MKCoordinateRegion newRegion;
        newRegion.center.latitude = [self.comment[@"lat"] floatValue];
        newRegion.center.longitude = [self.comment[@"lng"] floatValue];
        newRegion.span.latitudeDelta = 0.0005;
        newRegion.span.longitudeDelta = 0.0005;
        
        [self.myMapView setRegion:newRegion];
        
        [self addAnnotation];
    }
    
}

-(void) addAnnotation{
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake( [self.comment[@"lat"] floatValue], [self.comment[@"lng"] floatValue]);
    
    point.title = self.comment[@"user"] ;
    point.coordinate = coordinate;
    
    [self.myMapView addAnnotation: point];
}

- (IBAction)removeComent:(id)sender {
    
    UIAlertController * alert = [ UIAlertController alertControllerWithTitle: @"Remover Comentário" message:@"Realmente deseja remover o comentário?" preferredStyle: UIAlertControllerStyleActionSheet ];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Apagar" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSString *urlApagar =  [NSString stringWithFormat:@"https://teste-aula-ios.herokuapp.com/comments/%@.json", self.comment[@"id"]];
        
        [SVProgressHUD show];
        [manager DELETE:urlApagar
             parameters:nil
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    [SVProgressHUD dismiss];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [SVProgressHUD dismiss];
                    NSLog(@"Error: %@", error);
                }];
    }];
    
    UIAlertAction *cancelarAction = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:deleteAction];
    [alert addAction:cancelarAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
