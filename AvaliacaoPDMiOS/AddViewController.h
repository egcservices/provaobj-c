//
//  AddViewController.h
//  AvaliacaoPDMiOS
//
//  Created by EGC on 02/09/17.
//  Copyright © 2017 Ibratec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface AddViewController : UIViewController < UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtNome;
@property (weak, nonatomic) IBOutlet UITextField *txtComent;
@property (weak, nonatomic) IBOutlet UIImageView *imageSel;
@property (strong, nonatomic) CLLocationManager * locationManager;

@end
