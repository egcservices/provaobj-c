//
//  LogViewController.m
//  AvaliacaoPDMiOS
//
//  Created by EGC on 02/09/17.
//  Copyright © 2017 Ibratec. All rights reserved.
//

#import "LogViewController.h"
#import <AFNetworking.h>
#import <SVProgressHUD.h>

@interface LogViewController ()

@end

@implementation LogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.txtLogin.text = @"crystian@roadmaps.com.br";
    self.txtSenha.text = @"123123123";
    
    // Do any additional setup after loading the view.
}

- (IBAction)login:(id)sender {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *email = self.txtLogin.text;
    NSString *senha = self.txtSenha.text;
    
    
    NSDictionary *parameters = @{ @"user" : @{ @"email": email,
                                               @"password":senha } };
    
    [SVProgressHUD show];
    [manager  POST:@"https://teste-aula-ios.herokuapp.com/users/sign_in.json"
        parameters:parameters
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSLog(@"Login success");
               
               UIStoryboard *storyboard = self.storyboard;
               
               UIViewController * viewController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
               
               [self presentViewController:viewController animated:YES completion:nil];
               [SVProgressHUD dismiss];
               
           }           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               NSLog(@"Login failure");
               
               [SVProgressHUD dismiss];
               UIAlertController * alert = [UIAlertController
                                            alertControllerWithTitle:@"Login incorreto"
                                            message:@"Por favor verifique os campos e tente novamente!"
                                            preferredStyle:UIAlertControllerStyleAlert];
               
               UIAlertAction* ok = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
               
               [alert addAction:ok];
               
               [self presentViewController:alert animated:YES completion:nil];
           }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
