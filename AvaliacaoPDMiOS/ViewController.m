//
//  ViewController.m
//  AvaliacaoPDMiOS
//
//  Created by Crystian Leão on 02/09/17.
//  Copyright © 2017 Ibratec. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking.h>
#import "MyTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import <SVProgressHUD.h>
#import "DetailViewController.h"

@interface ViewController ()

@property (strong) NSMutableArray *comments;
@property (nonatomic) int page;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.comments = [NSMutableArray arrayWithArray: @[]];
    self.page = 1;
    
    // Isso aqui diz pra o iOS não criar espaços em cima das scrollviews, por causa do navigation bar
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [self recarregarDados];
}

- (IBAction)back:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)refresh:(id)sender {
    [self recarregarDados];
}

- (void) recarregarDados {
    self.page = 1;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [SVProgressHUD show];
    [manager GET:@"https://teste-aula-ios.herokuapp.com/comments.json"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             [SVProgressHUD dismiss];
             self.comments = [NSMutableArray arrayWithArray:responseObject];
             [self.myTable reloadData];
         }    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [SVProgressHUD dismiss];
             NSLog(@"Error: %@", error);
         }];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.comments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyTableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    
    NSDictionary *comment = self.comments[indexPath.row];
    myCell.lblUser.text    = comment[@"user"];
    myCell.lblComent.text = comment[@"content"];
    
    [myCell.img setImageWithURL:[NSURL URLWithString:comment[@"image"]]
               placeholderImage:[UIImage imageNamed:@"vangogh"]];
    
    if (indexPath.row == [self.comments count] - 1) {
        // carrega mais celulas após a última célula
        [self carregaProximaPagina];
    }
    
    return myCell;
}

- (void) carregaProximaPagina {
    if (self.page == -1)
        return;
    self.page += 1;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:@"https://teste-aula-ios.herokuapp.com/comments.json"
      parameters:@{@"page":@(self.page)}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             if ([responseObject count] == 0) {
                 // This will stop pagination from happening
                 self.page = -1;
             } else {
                 [self.comments addObjectsFromArray:responseObject];
                 [self.myTable reloadData];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *myObj = self.comments[indexPath.row];
    
    DetailViewController * viewDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    
    viewDetail.comment = myObj;
    
    [self.navigationController pushViewController:viewDetail animated:true];
    //    [self presentViewController:viewDetail animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
