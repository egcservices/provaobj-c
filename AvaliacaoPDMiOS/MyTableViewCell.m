//
//  MyTableViewCell.m
//  AvaliacaoPDMiOS
//
//  Created by EGC on 02/09/17.
//  Copyright © 2017 Ibratec. All rights reserved.
//

#import "MyTableViewCell.h"

@implementation MyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
